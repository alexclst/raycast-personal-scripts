# Raycast Scripts

This folder contains my personal script commands used by [Raycast](https://www.raycast.com). See `script-command.template.sh` for a template of what these are supposed to have.