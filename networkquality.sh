#!/bin/bash

# myip
#
# Runs Apple's network quality tool within Raycast
#
# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Network Quality
# @raycast.mode fullOutput
# Optional parameters:
# @raycast.icon 🌎
# @raycast.packageName Personal Scripts

networkQuality