#!/bin/bash

# @raycast.schemaVersion 1
# @raycast.title Prettify JSON
# @raycast.packageName Personal Scripts
# @raycast.mode pipe
# @raycast.inputType text
# @raycast.icon 🔨

python3 -m json.tool --indent 2